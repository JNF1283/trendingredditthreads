let createCORSRequest = (method, url) => {
  let xhr = new XMLHttpRequest();
  if ("withCredentials" in xhr) {
    // Most browsers.
    xhr.open(method, url, true);
  } else if (typeof XDomainRequest != "undefined") {
    // IE8 & IE9
    xhr = new XDomainRequest();
    xhr.open(method, url);
  } else {
    // CORS not supported.
    xhr = null;
  }
  return xhr;
};

// Top trending threads in /r/javascript
let url = 'https://www.reddit.com/r/javascript/hot.json';
let method = 'GET';
let xhr = createCORSRequest(method, url);

xhr.onload = function () {
  if(this.status === 200) {
    const response = JSON.parse(this.responseText);
    let title = '';
    let i;
    for (i = 0; i < 10; i++) {
      title += "<a href=\"" + "https://www.reddit.com" + response.data.children[i].data.permalink + "\" target=\"_blank\" class='title'>" + "<div class='thread'>" + response.data.children[i].data.title + "</div>" + "</a>" + "<br>";
    }
    document.getElementById('threads').innerHTML = title;
  } else {
    document.getElementById('threads').innerHTML = "<h2>" + "Please refresh the page.  If the page doesn't reload, please try again later." + "</h2>"
  }
};

xhr.onerror = function () {
  console.log('An error has occurred.');
};

xhr.send();
